<?php
/**
 * @file
 * Enables modules and site configuration for Migrate Testing site installation.
 */

/**
 * Implements hook_profile_details().
 */
function mt_profile_details() {
  return array(
    'name' => 'Migrate Testing',
    'description' => 'Migrate Testing.',
  );
}

/**
 * Implements hook_profile_modules().
 */
function mt_profile_modules() {
  $modules = array(
    // Core
    'aggregator',
    'blog',
    'blogapi',
    'book',
    'color',
    'comment',
    'contact',
    'translation',
    'dblog',
    'help',
    'locale',
    'menu',
    'openid',
    'path',
    'php',
    'ping',
    'poll',
    'profile',
    'search',
    'statistics',
    'syslog',
    'taxonomy',
    'throttle',
    'tracker',
    'trigger',
    'update',
    'upload',
    'forum',
    // Contrib.
    'content',
    'content_copy',
    'content_permissions',
    'fieldgroup',
    'filefield',
    'filefield_meta',
    'getid3',
    'install_profile_api',
    'nodereference',
    'number',
    'optionwidgets',
    'text',
    'userreference',
    );
  return $modules;
}

/**
 * Implements hook_profile_task_list().
 */
function md_task_list() {
  $tasks = array(
    'mt-install' => st('Migrate Testing installation'),
  );
  return $tasks;
}

/**
 * Implements hook_profile_tasks().
 */
function mt_profile_tasks(&$task, $url) {
  // Add include for install_profile_api support.
  install_include(mt_profile_modules());
  $output = '';
  if ($task == 'profile') {
    $task = 'mt-install';
  }
  if ($task == 'mt-install') {
    mt_config_content_types();
    mt_config_vars();
    mt_config_roles();
    mt_configure_permissions();
    mt_config_cck_fields();
    mt_config_nodes();
    $task = 'profile-finished';
  }
  return $output;
}

/**
 * Configure variables.
 */
function mt_config_vars() {
  variable_set('actions_max_stack', 35);
  variable_set('admin_compact_mode', FALSE);
  variable_set('aggregator_allowed_html_tags', '<a> <b> <br /> <dd> <dl> <dt> <em> <i> <li> <ol> <p> <strong> <u> <ul>');
  variable_set('aggregator_clear', 9676800);
  variable_set('aggregator_fetcher', 'aggregator');
  variable_set('aggregator_parser', 'aggregator');
  variable_set('aggregator_processors', array('aggregator'));
  variable_set('aggregator_summary_items', 3);
  variable_set('aggregator_teaser_length', 600);
  variable_set('allowed_html_1', '<a> <em> <strong> <cite> <code> <ul> <ol> <li> <dl> <dt> <dd>');
  variable_set('allow_insecure_uploads', 1);
  variable_set('anonymous', 'Guest');
  variable_set('book_allowed_types', array('book'));
  variable_set('book_block_mode', 'all pages');
  variable_set('book_child_type', 'book');
  variable_set('cache', 1);
  variable_set('cache_lifetime', 0);
  variable_set('comment_anonymous_page', 0);
  variable_set('comment_anonymous_story', 1);
  variable_set('comment_controls_page', 3);
  variable_set('comment_controls_story', 3);
  variable_set('comment_default_mode_page', 4);
  variable_set('comment_default_mode_story', 2);
  variable_set('comment_default_order_page', 1);
  variable_set('comment_default_order_story', 1);
  variable_set('comment_default_per_page_page', 50);
  variable_set('comment_default_per_page_story', 70);
  variable_set('comment_form_location_page', 0);
  variable_set('comment_form_location_story', 0);
  variable_set('comment_page', 0);
  variable_set('comment_preview_page', 1);
  variable_set('comment_preview_story', 0);
  variable_set('comment_story', 2);
  variable_set('comment_subject_field_page', 1);
  variable_set('comment_subject_field_story', 0);
  variable_set('contact_default_status', 1);
  variable_set('contact_hourly_threshold', 3);
  variable_set('cron_threshold_error', 1209600);
  variable_set('cron_threshold_warning', 172800);
  variable_set('date_format_long', '\\L\\O\\N\\G l, F j, Y - H:i');
  variable_set('date_format_medium','\\M\\E\\D\\I\\U\\M D, m/d/Y - H:i');
  variable_set('date_format_short','\\S\\H\\O\\R\\T m/d/Y - H:i');
  variable_set('dblog_row_limit', 1000);
  variable_set('feed_default_items', 10);
  variable_set('file_description_length', 128);
  variable_set('file_description_type', 'textfield');
  variable_set('file_directory_path', 'core/modules/simpletest/files');
  variable_set('file_directory_temp', 'files/temp');
  variable_set('file_icon_directory','sites/default/files/icons');
  variable_set('filter_allowed_protocols', array('http', 'https', 'ftp', 'news', 'nntp', 'tel', 'telnet', 'mailto', 'irc', 'ssh', 'sftp', 'webcal', 'rtsp'));
  variable_set('filter_html_help_1', 1);
  variable_set('filter_html_nofollow_1', 0);
  variable_set('filter_url_length_1', 72);
  variable_set('forum_block_num_0', 5);
  variable_set('forum_block_num_1', 5);
  variable_set('forum_hot_topic', 15);
  variable_set('forum_nav_vocabulary', 1);
  variable_set('forum_order', 1);
  variable_set('forum_per_page', 25);
  variable_set('image_jpeg_quality', 75);
  variable_set('image_toolkit', 'gd');
  variable_set('locale_cache_strings', 1);
  variable_set('locale_js_directory', 'languages');
  variable_set('menu_override_parent_selector', FALSE);
  variable_set('minimum_word_size', 3);
  variable_set('node_admin_theme',0);
  variable_set('node_options_test_event', array('sticky', 'revision'));
  variable_set('node_options_test_page', array('status', 'promote', 'sticky'));
  variable_set('node_options_test_planet', array('sticky', 'revision'));
  variable_set('node_options_test_story', array('status', 'promote'));
  variable_set('node_preview', 0);
  variable_set('node_rank_comments', 5);
  variable_set('node_rank_promote', 0);
  variable_set('node_rank_recent', 0);
  variable_set('node_rank_relevance', 2);
  variable_set('node_rank_sticky', 8);
  variable_set('node_rank_views', 1);
  variable_set('overlap_cjk', TRUE);
  variable_set('page_compression', TRUE);
  variable_set('preprocess_css', FALSE);
  variable_set('preprocess_js', FALSE);
  variable_set('search_cron_limit', 100);
  variable_set('simpletest_clear_results', TRUE);
  variable_set('simpletest_httpauth_method', 1);
  variable_set('simpletest_httpauth_password', 'N;');
  variable_set('simpletest_httpauth_username', 'N;');
  variable_set('simpletest_verbose', TRUE);
  variable_set('site_403', 'user');
  variable_set('site_404', 'page-not-found');
  variable_set('site_frontpage', 'node');
  variable_set('site_mail', 'site_mail@example.com');
  variable_set('site_name', 'site_name');
  variable_set('site_offline', 0);
  variable_set('site_offline_message', 'Drupal is currently under maintenance. We should be back shortly. Thank you for your patience.');
  variable_set('site_slogan', 'Migrate rocks');
  variable_set('statistics_block_top_all_num', 0);
  variable_set('statistics_block_top_day_num', 0);
  variable_set('statistics_block_top_last_num', 0);
  variable_set('statistics_count_content_views', 0);
  variable_set('statistics_enable_access_log', 0);
  variable_set('statistics_flush_accesslog_timer', 259200);
  variable_set('syslog_facility', 128);
  variable_set('syslog_identity', 'drupal');
  variable_set('taxonomy_override_selector', FALSE);
  variable_set('taxonomy_terms_per_page_admin', 100);
  variable_set('teaser_length', 456);
  variable_set('theme_settings', array(
    'toggle_logo' => 1,
    'toggle_name' => 1,
    'toggle_slogan' => 0,
    'toggle_mission' => 1,
    'toggle_node_user_picture' => 0,
    'toggle_comment_user_picture' => 0,
    'toggle_search' => 0,
    'toggle_favicon' => 1,
    'toggle_primary_links' => 1,
    'toggle_secondary_links' => 1,
    'toggle_node_info_test' => 1,
    'toggle_node_info_something' => 1,
    'default_logo' => 1,
    'logo_path' => '',
    'logo_upload' => '',
    'default_favicon' => 1,
    'favicon_path' => '',
    'favicon_upload' => '',
    'toggle_node_info_test_page' => 1,
    'toggle_node_info_test_story' => 1,
    'toggle_node_info_test_event' => 1,
    'toggle_node_info_test_planet' => 1,
  ));
  variable_set('update_check_frequency', 7);
  variable_set('update_fetch_url', 'http://updates.drupal.org/release-history');
  variable_set('update_max_fetch_attempts', 2);
  variable_set('update_notification_threshold', 'all');
  variable_set('update_notify_emails', array());
  variable_set('upload_article', FALSE);
  variable_set('upload_page', TRUE);
  variable_set('upload_story', TRUE);
  variable_set('user_email_verification', FALSE);
  variable_set('user_mail_password_reset_body', '!username,\n\nA request to reset the password for your account has been made at !site.\n\nYou may now log in to !uri_brief by clicking on this link or copying and pasting it in your browser:\n\n!login_url\n\nThis is a one-time login, so it can be used only once. It expires after one day and nothing will happen if it\'s not used.\n\nAfter logging in, you will be redirected to !edit_uri so you can change your password.');
  variable_set('user_mail_password_reset_subject', 'Replacement login information for !username at !site');
  variable_set('user_mail_register_admin_created_body', '!username,\n\nA site administrator at !site has created an account for you. You may now log in to !login_uri using the following username and password:\n\nusername: !username\npassword: !password\n\nYou may also log in by clicking on this link or copying and pasting it in your browser:\n\n!login_url\n\nThis is a one-time login, so it can be used only once.\n\nAfter logging in, you will be redirected to !edit_uri so you can change your password.\n\n\n--  !site team');
  variable_set('user_mail_register_admin_created_subject', 'An administrator created an account for you at !site');
  variable_set('user_mail_register_no_approval_required_body', '!username,\n\nThank you for registering at !site. You may now log in to !login_uri using the following username and password:\n\nusername: !username\npassword: !password\n\nYou may also log in by clicking on this link or copying and pasting it in your browser:\n\n!login_url\n\nThis is a one-time login, so it can be used only once.\n\nAfter logging in, you will be redirected to !edit_uri so you can change your password.\n\n\n--  !site team');
  variable_set('user_mail_register_no_approval_required_subject', 'Account details for !username at !site');
  variable_set('user_mail_status_activated_body', '!username,\n\nYour account at !site has been activated.\n\nYou may now log in by clicking on this link or copying and pasting it in your browser:\n\n!login_url\n\nThis is a one-time login, so it can be used only once.\n\nAfter logging in, you will be redirected to !edit_uri so you can change your password.\n\nOnce you have set your own password, you will be able to log in to !login_uri in the future using:\n\nusername: !username\n');
  variable_set('user_mail_status_activated_notify', FALSE);
  variable_set('user_mail_status_activated_subject', 'Account details for !username at !site (approved)');
  variable_set('user_mail_status_blocked_body', '!username,\n\nYour account on !site has been blocked.');
  variable_set('user_mail_status_blocked_notify', TRUE);
  variable_set('user_mail_status_blocked_subject', 'Account details for !username at !site (blocked)');
  variable_set('user_mail_status_deleted_body', '!username,\n\nYour account on !site has been deleted.');
  variable_set('user_mail_status_deleted_subject', 'Account details for !username at !site (deleted)');
  variable_set('user_mail_user_mail_register_pending_approval_body', '!username,\n\nThank you for registering at !site. Your application for an account is currently pending approval. Once it has been approved, you will receive another email containing information about how to log in, set your password, and other details.\n\n\n--  !site team');
  variable_set('user_mail_user_mail_register_pending_approval_subject', 'Account details for !username at !site (pending admin approval)');
  variable_set('user_register', 0);
  variable_set('user_signatures', 1);
}

/**
 * Configure content types.
 */
function mt_config_content_types() {
  $content_types = array(
    array(
      'type' => 'article',
      'name' => 'Article',
      'description' => "An <em>article</em>, content type.",
      'body_label' => 'Body',
      'orig_type' => 'story',
    ),
    array(
      'type' => 'company',
      'name' => 'Company',
      'module' => 'node',
      'description' => 'Company node type',
      'help' => '',
      'has_title' => 1,
      'title_label' => 'Name',
      'has_body' => 1,
      'body_label' => 'Description',
      'min_word_count' => 20,
      'custom' => 0,
      'modified' => 0,
      'locked' => 0,
      'orig_type' => 'company',
    ),
    array(
      'type' => 'employee',
      'name' => 'Employee',
      'description' => 'Employee node type',
      'title_label' => 'Name',
      'body_label' => 'Bio',
      'min_word_count' => 20,
      'custom' => 0,
      'modified' => 0,
      'orig_type' => 'employee',
    ),
    array(
      'type' => 'page',
      'name' => 'Page',
      'description' => "A <em>page</em>, similar in form to a <em>story</em>, is a simple method for creating and displaying information that rarely changes, such as an \"About us\" section of a website. By default, a <em>page</em> entry does not allow visitor comments and is not featured on the site's initial home page.",
      'has_title' => '1',
      'title_label' => 'Title',
      'has_body' => '1',
      'body_label' => 'Body',
      'min_word_count' => '0',
      'custom' => '1',
      'modified' => '1',
      'locked' => '0',
      'orig_type' => 'page',
    ),
    array(
      'type' => 'sponsor',
      'name' => 'Sponsor',
      'description' => 'Sponsor node type',
      'title_label' => 'Name',
      'has_body' => 0,
      'body_label' => 'Body',
      'custom' => 0,
      'modified' => 0,
    ),
    array(
      'type' => 'story',
      'name' => 'Story',
      'description' => "A <em>story</em>, similar in form to a <em>page</em>, is ideal for creating and displaying content that informs or engages website visitors. Press releases, site announcements, and informal blog-like entries may all be created with a <em>story</em> entry. By default, a <em>story</em> entry is automatically featured on the site's initial home page, and provides the ability to post comments.",
      'body_label' => 'Body',
      'orig_type' => 'story',
    ),
    array(
      'type' => 'test_event',
      'name' => 'Migrate test event',
      'description' => "test event description here",
      'help' => 'help text here',
      'title_label' => 'Event Name',
      'has_body' => 1,
      'body_label' => 'Body',
      'orig_type' => 'event',
    ),
    array(
      'type' => 'test_page',
      'name' => 'Migrate test page',
      'description' => "A <em>page</em>, similar in form to a <em>story</em>, is a simple method for creating and displaying information that rarely changes, such as an \"About us\" section of a website. By default, a <em>page</em> entry does not allow visitor comments and is not featured on the site's initial home page.",
      'body_label' => 'This is the body field label',
      'orig_type' => 'page',
    ),
    array(
      'type' => 'test_planet',
      'name' => 'Migrate test planet',
      'description' => "A <em>story</em>, similar in form to a <em>page</em>, is ideal for creating and displaying content that informs or engages website visitors. Press releases, site announcements, and informal blog-like entries may all be created with a <em>story</em> entry. By default, a <em>story</em> entry is automatically featured on the site's initial home page, and provides the ability to post comments.",
      'has_body' => 0,
      'body_label' => 'Body',
      'orig_type' => 'test_planet',
    ),
    array(
      'type' => 'test_story',
      'name' => 'Migrate test story',
      'description' => "A <em>story</em>, similar in form to a <em>page</em>, is ideal for creating and displaying content that informs or engages website visitors. Press releases, site announcements, and informal blog-like entries may all be created with a <em>story</em> entry. By default, a <em>story</em> entry is automatically featured on the site's initial home page, and provides the ability to post comments.",
      'has_body' => 0,
      'body_label' => 'Body',
      'orig_type' => 'story',
    ),
  );
  foreach($content_types as $content_type) {
    install_add_content_type($content_type);
  }
}

/**
 * Configure roles.
 */
function mt_config_roles() {
  $roles = array(
    'migrate test role 1',
    'migrate test role 2',
    'migrate test role 3 that is longer than thirty two characters',
  );
  foreach ($roles as $name) {
    db_query("INSERT INTO {role} (name) VALUES ('%s')", $name);
  }
}

function mt_configure_permissions() {
  // Update permissions for default profile user roles.
  db_query("UPDATE {permission} set perm = 'migrate test anonymous permission' WHERE rid = 1");
  db_query("UPDATE {permission} set perm = 'migrate test authenticated permission' WHERE rid = 2");

  // migrate test role 1
  db_query("INSERT INTO {permission} (rid, perm) VALUES (%d, '%s')", 3, 'migrate test role 1 test permission');

  // migrate test role 2
  $perms = array(
    'migrate test role 2 test permission',
    'use PHP for block visibility',
    'administer site-wide contact form',
    'post comments without approval',
    'edit own blog entries',
    'edit any blog entry',
    'delete own blog entries',
    'delete any blog entry',
    'create forum topics',
    'delete any forum topic',
    'delete own forum topics',
    'edit any forum topic',
    'edit own forum topics',
    'administer nodes',
  );
  db_query("INSERT INTO {permission} (rid, perm) VALUES (%d, '%s')", 4, implode(', ', $perms));
}

/**
 * Configure cck fields.
 */
function mt_config_cck_fields() {
  $fields = array(
    array(
      'field_name' => 'field_test',
      'type_name' => 'story',
      'weight' => 1,
      'label' => 'Text Field',
      'widget_type' => 'text_textfield',
      'widget_settings' => serialize(array(
          'rows' => 5,
          'size' => '60',
          'default_value' => array(
            0 => array(
              'value' => 'text for default value',
              '_error_element' => 'default_value_widget][field_test][0][value',
            ),
          ),
          'default_value_php' => NULL,
        )),
      'display_settings' => serialize(array(
          'weight' => 1,
          'parent' => '',
          'label' => array(
            'format' => 'above',
          ),
          1 => array(
            'format' => 'default',
            'exclude' => 0,
          ),
          'teaser' => array(
            'format' => 'trimmed',
            'exclude' => 0,
          ),
          'full' => array(
            'format' => 'default',
            'exclude' => 0,
          ),
          4 => array(
            'format' => 'trimmed',
            'exclude' => 0,
          ),
          5 => array(
            'format' => 'default',
            'exclude' => 1,
          ),
        )),
      'description' => 'An example text field.',
      'properties' => array(
        'module' => 'text',
        'type' => 'text',
      ),
    )
  );
  foreach ($fields as $field) {
    install_create_field($field['type_name'], $field['field_name'], $field['widget_type'], $field['label'], $field['properties']);
  }
  // Add existing fields that span more than one content type.
  foreach ($fields as $field) {
    install_add_existing_field($field['type_name'], $field['field_name'], $field['widget_type']);
  }
}

/**
 * Configure nodes.
 */
function mt_config_nodes() {
$nodes = array(
  array(
    'title' => 'Test title',
    'body' => NULL,
    'properties' => array(
      'nid' => 1,
      'vid' => 1,
      'type' => 'story',
      'language' => '',
      'uid' => 1,
      'status' => 1,
      'created' => 1388271197,
      'changed' => 1390095701,
      'comment' => 0,
      'promote' => 0,
      'moderate' => 0,
      'sticky' => 0,
      'tnid' => 0,
      'translate' => 0,
      'field_test' => array(
        0 => array(
          'value' => 'This is a shared text field',
          '_error_element' => 'default_value_widget][field_test][0][value',
        ),
      ),
    ),
  ),
  array(
    'title' => 'Test title 2',
    'body' => NULL,
    'properties' => array(
      'nid' => 2,
      'vid' => 3,
      'type' => 'story',
      'language' => '',
      'uid' => 1,
      'status' => 1,
      'created' => 1388271197,
      'changed' => 1390095701,
      'comment' => 0,
      'promote' => 0,
      'moderate' => 0,
      'sticky' => 0,
      'tnid' => 0,
      'translate' => 0,
    ),
  ),
  array(
    'title' => 'Test planet title 3',
    'body' => NULL,
    'properties' => array(
      'nid' => 3,
      'vid' => 4,
      'type' => 'test_planet',
      'language' => '',
      'uid' => 1,
      'status' => 1,
      'created' => 1388271527,
      'changed' => 1390096401,
      'comment' => 0,
      'promote' => 0,
      'moderate' => 0,
      'sticky' => 0,
      'tnid' => 0,
      'translate' => 0,
      ),
    ),
  );
  foreach ($nodes as $node) {
    install_create_node($node['title'], $node['body'], $node['properties']);
  }
}

/**
 * Sets English as the default language.
 */
function system_form_install_select_locale_form_alter(&$form, $form_state) {
  $form['locale']['en']['#value'] = 'en';
}

/**
 * Sets Migrate Testing as the default install profile.
 */
function system_form_install_select_profile_form_alter(&$form, $form_state) {
  foreach($form['profile'] as $key => $element) {
    $form['profile'][$key]['#value'] = 'mt';
  }
}
