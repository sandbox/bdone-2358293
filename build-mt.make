api = 2
core = 6.x

includes[] = drupal-org-core.make

projects[mt][type] = "profile"
projects[mt][download][type] = "git"
projects[mt][download][branch] = "6.x-1.x"
projects[mt][download][url] = "http://git.drupal.org:sandbox/bdone/2358293.git"
