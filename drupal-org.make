api = 2
core = 6.x

projects[cck][version] = "2.9"
projects[cck][type] = "module"
projects[cck][subdir] = "contrib"

projects[filefield][version] = "3.13"
projects[filefield][type] = "module"
projects[filefield][subdir] = "contrib"

projects[getid3][version] = "1.5"
projects[getid3][type] = "module"
projects[getid3][subdir] = "contrib"

projects[install_profile_api][version] = "2.2"
projects[install_profile_api][type] = "module"
projects[install_profile_api][subdir] = "contrib"
